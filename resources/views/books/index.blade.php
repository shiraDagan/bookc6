
<h1>This is your books list</h1>
@extends('layouts.app')
         @section('content')
<table>
  <thead>
        <tr>
            <th> NameOfBook</th>
            <th>  Author   </th>

        </tr>
    </thead>
    <tbody>
         @foreach($books as $book)
       
 

          <tr>
              <td>
              @if ($book->status)
                <input type = 'checkbox' id ="{{$book->id}}" checked>
              @else
                <input type = 'checkbox' id ="{{$book->id}}">
               @endif
               <a href = "{{route('books.edit',$book->id)}}">{{$book->title}} </a>
              </td>
              <td>  {{$book->author}} </td>
              
          </tr>
          
    
         @endforeach
         <a href = "{{route('books.create')}}">   create a new book </a>
         <script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
             console.log(event.target.id)//cheak thr right id when click cheak
               $.ajax({
                   url: "{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   data:JSON.stringify( 
                     {'status':event.target.checked,_token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  

         
         
   </tbody>
   <style>
table, th, td {
  border: 1px solid black;
}
</style>
</table>
@endsection

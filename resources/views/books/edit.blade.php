@extends('layouts.app')
@section('content')


<h1>Edit book</h1>
<form method = 'post' action = "{{action('BookController@update', $books->id)}}" >
@csrf    
@method('PATCH')  

    
<div class = "form-group">    
    <label for = "title">Book to Update </label>
    <input type = "text" class = "form-control" name = "title" value = "{{$books->title}}">
</div>
<div class = "form-group">   
<label for = "author">Who is the author ? </label>
    <input type = "text" class = "form-control" name = "author">
</div>

<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Update">
</div>

</form>

<form method = 'post' action = "{{action('BookController@update', $books->id)}}" >
@csrf    
@method('DELETE')    
    
<div class = "form-group">    
    <label for = "title">Book to Delete </label>
    <input type = "text" class = "form-control" name = "title" value = "{{$books->title}}">

<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Delete">
</div>

</form>

@endsection

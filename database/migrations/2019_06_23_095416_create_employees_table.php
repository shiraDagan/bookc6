<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            //$table->bigIncrements('employee');
            $table->timestamps();
            $table->integer('manager_id')->unsignedBiginteger();//יכול להופיע כמה פעמים לכן אינו מפתח
            $table->integer('employee_id')->unsignedBiginteger()->primary();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}

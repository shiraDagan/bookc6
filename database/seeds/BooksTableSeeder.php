<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
                [
            'title' => 'War and Peace',
            'user_id' => '1',
            'author' => 'Leo Tolstoy',
            'created_at' => date('Y-m-d G:i:s'),
            ],
        
            [
                'title' => 'Song of Solomon',
                'user_id' => '2',
                'author' => 'Toni Morrison',
                'created_at' => date('Y-m-d G:i:s'),
            ],
        
            [
                    'title' => 'Ulysses',
                    'user_id' => '3',
                    'author' => 'James Joyce',
                    'created_at' => date('Y-m-d G:i:s'),
            ],

            [
                'title' => 'The Shadow of the Wind',
                'user_id' => '4',
                'author' => 'Carlos Ruiz Zafon',
                'created_at' => date('Y-m-d G:i:s'),
        ],

        [
            'title' => 'The Lord of the Rings',
            'user_id' => '5',
            'author' => 'J.R.R. Tolkien',
            'created_at' => date('Y-m-d G:i:s'),
    ]
            ]);


        }
}

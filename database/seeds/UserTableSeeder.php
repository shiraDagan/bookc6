<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'Shira',
                    'email' => 'shira@gmail.com',
                    'password' =>'12345678',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                 'name' => 'David',
                 'email' => 'david@gmail.com',
                 'password' =>'12345678',
                 'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                 'name' => 'Dan',
                 'email' => 'dan@jack.com',
                 'password' =>'12345678',
                 'created_at' => date('Y-m-d G:i:s'),
                ],
            ]);
    }
}

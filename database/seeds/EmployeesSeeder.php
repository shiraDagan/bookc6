<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'Ariel',
                    'email' => 'ariel@gmail.com',
                    'password' =>Hash::make('123456789'),
                    'created_at' => date('Y-m-d G:i:s'),
                    'role' => 'employee',
                ],
                [
                 'name' => 'Nava',
                 'email' => 'nava@gmail.com',
                 'password' =>Hash::make('123456789'),
                 'created_at' => date('Y-m-d G:i:s'),
                 'role' => 'employee',
                ],
                [
                 'name' => 'Sharon',
                 'email' => 'sharon@jack.com',
                 'password' =>Hash::make('123456789'),
                 'created_at' => date('Y-m-d G:i:s'),
                 'role' => 'employee',
                ],
            ]);

    }
}

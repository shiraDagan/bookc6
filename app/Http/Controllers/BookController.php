<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;




use App\Book;
use App\User;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //get all books for user 1
      // $books= Book::all(); //שאילתא ששולפת את כל הנתונים. 
    //  $id = 1;
    // $id = Auth::id();
    //   $books = User::find($id)->books;
    //    return view('books.index',compact('books'));
    $id = Auth::id();
    if (Gate::denies('manager')) {
        $boss = DB::table('employees')->where('employee',$id)->first();
        $id = $boss->manager;
    }
    $user = User::find($id);
    $books = $user->books;
    return view('books.index', compact('books'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if (Gate::denies('manager')) {
        abort(403,"Sorry you are not allowed to create todos..");
    }
        return view ('books.create');
        

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $book = new Book();
         //$id = 1;
         $id = Auth::id();
         $book->title = $request->title;
         $book->author = $request->author;
         $book->status = 0; 
         $book->user_id = $id;
         $book->save();
        return redirect('books');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $books = Book::find($id);
         return view('books.edit', compact('books'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::find($id);
        if(!$book->user->id == Auth::id()) return(redirect('books'));
        $book->update($request->except(['_token']));
        if($request->ajax()){

          return Response::json(array('result' =>'success','status'=>$request->status),200);  
        }
       // $books->update($request -> all());
        return redirect('books');
      
      //return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $book = Book::find($id);
        // if(!$books->user->id == Auth::id()) return(redirect('books'));
         $book->delete(); 
         return redirect('books');   
    }
}
